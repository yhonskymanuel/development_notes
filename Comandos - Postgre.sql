*******************************************************************************************************
# Ver la versión de PostgreSQL
SELECT version();
C:\Users\%USERNAME%\AppData\Roaming\pgAdmin
*******************************************************************************************************
# Comandos de navegación y consulta de información
\l	=> para listar todas las bases de datos existentes
\dt	=> para listar todas las tablas que existen en una base de datos
\dn	=> Listar los esquemas de la base de datos actual
\df	=> Listar las funciones disponibles de la base de datos actual
\dv	=> Listar las vistas de la base de datos actual
\du	=> Listar los usuarios y sus roles de la base de datos actual
\c	=> Para cambiar a otra base de datos en la consola
\d	=> Para describir la base de datos o la tabla
\d <nombre_tabla> Describir una tabla

# Comandos de ayuda
\h	=> para ver todas las funciones que podemos ejecutar en PostgreSQL
\h <palabra_sql> => Muestra cómo usar la función
\?	=> Con el cual podemos ver la lista de todos los comandos disponibles en consola, comandos que empiezan con backslash ()
Ctrl + c =>  cancela lo que se estaba mostrando

# Comandos de inspección y ejecución
\g	=> Volver a ejecutar el comando ejecutando justo antes
\s	=> Ver el historial de comandos ejecutados
\s <nombre_archivo> => Si se quiere guardar la lista de comandos ejecutados en un archivo de texto plano
\i <nombre_archivo> => Ejecutar los comandos desde un archivo
\e	=> Permite abrir un editor de texto plano, escribir comandos y ejecutar en lote. \e abre el editor de texto, escribir allí todos los comandos, luego guardar los cambios y cerrar, al cerrar se ejecutarán todos los comandos guardados.
\ef	=> Equivalente al comando anterior pero permite editar también funciones en PostgreSQL
\q	=> Cerrar la consola

# Para debug y optimización
\timing => inicializar el contador de tiempo para que la consola muestre en cada ejecución cuanto se demoro en ejecutar ese comando
*******************************************************************************************************
# Para ver la ruta de los archivos de configuración
SHOW config_file;

Los archivos principales de configuración son tres:

postgreql.conf
pg.hba.conf
pg_ident.conf

Si se modifica algunos de estos archivos se debe reiniciar el server de BD para que los cambios apliquen
*******************************************************************************************************
SELECT * FROM pg_stat_activity;
SELECT pg_cancel_backend(<pid>); 
SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = 'basededatos';
*******************************************************************************************************
# Devuelve la fecha actual del servidor
SELECT current_date;
SELECT current_time;
SELECT current_timestamp;
SELECT EXTRACT(YEAR FROM CURRENT_DATE);
*******************************************************************************************************
# Muestra los parámetros para crear roles
\h CREATE ROLE => MUESTRA LOS PARAQMETROS PARA CREAR ROLES

# Crear roles
CREATE ROLE usuario_consulta;
CREATE USER usuario_consulta;

# Lista de todos los usuarios y sus permisos
\dg

# Gestión de roles
ALTER ROLE usuario_consulta WITH LOGIN;
ALTER ROLE usuario_consulta WITH SUPERUSER;
ALTER ROLE usuario_consulta WITH PASSWORD 'passwor';
DROP ROLE usuario_consulta

Para establcer permsisos a una tabla en particular se puede utilizar el grant_wizar y establecer el ususario y los permisos sobre dicha tabla
*******************************************************************************************************
# ON CONFLICT DO
INSERT INTO public.estacion(
	id, nombre, direccion)
	VALUES (1, 'Caricuao', 'Zoologico')
	ON CONFLICT(id) DO UPDATE SET nombre='Caricuao',direccion='Zoologico';
*******************************************************************************************************
# RETURNING
INSERT INTO public.estacion(
	 nombre, direccion)
	VALUES ('Plaza Venezuela', 'Caracas')
RETURNING *;
*******************************************************************************************************
# LIKE / ILIKE

SELECT nombre FROM public.pasajero
WHERE nombre ILIKE 'o%';
*******************************************************************************************************
# IS / IS NOT
SELECT * FROM pasajero
LEFT JOIN viaje ON (viaje.id_pasajero=pasajero.id)
WHERE viaje.id ISNULL;
*******************************************************************************************************
# COALESCE
SELECT id, COALESCE(nombre,'No aplica') AS nombre, direccion_residencia, fecha_nacimiento
	FROM public.pasajero
	WHERE id=1;
	
*******************************************************************************************************
# NULLIF
SELECT NULLIF (0,0);
*******************************************************************************************************
# GREATEST & LEAST
SELECT GREATEST (0,1,2,3,4,5,6,7,8,9);
SELECT LEAST (0,1,2,3,4,5,6,7,8,9);
*******************************************************************************************************
# BLOQUES ANNONIMOS
SELECT id, nombre, direccion_residencia, fecha_nacimiento,
	CASE
	WHEN (EXTRACT(YEAR FROM CURRENT_DATE) - EXTRACT(YEAR FROM fecha_nacimiento)) >= 18 THEN
	'Si'
	ELSE
	'No'
	END AS mayor_edad
FROM public.pasajero;
*******************************************************************************************************
--CREATE EXTENSION dblink;

SELECT * FROM
dblink('dbname=transporte_vip_remota
	   port=5433 
	   host=127.0.0.1
	   user=usuario_consulta
	   password=etc123' ,
	   'SELECT id, fecha FROM vip') 
	   AS datos_remotos(id integer, fecha date);
	   
SELECT * FROM pasajero
JOIN
dblink('dbname=transporte_vip_remota
	   port=5433 
	   host=127.0.0.1
	   user=usuario_consulta
	   password=etc123' ,
	   'SELECT id, fecha FROM vip') 
	   AS datos_remotos(id integer, fecha date)
ON (pasajero.id = datos_remotos.id);

SELECT * FROM pasajero
JOIN
dblink('dbname=transporte_vip_remota
	   port=5433 
	   host=127.0.0.1
	   user=usuario_consulta
	   password=etc123' ,
	   'SELECT id, fecha FROM vip') 
	   AS datos_remotos(id integer, fecha date)
USING (id);

para transaccione shay qye desactivar el autocommit de postgre,
En los rollback se puede utilizar savepoint para decir hasta que punto llega el rollback

BEGIN;
	
INSERT INTO public.tren(
	 modelo, capacidad)
	VALUES ( 'Modelo transaccion 2', '1234');
	
INSERT INTO public.estacion(
	id, nombre, direccion)
	VALUES (3, 'Estacion Transaccion 2', 'direcc transaccion 2');

COMMIT;


--CREATE EXTENSION fuzzystrmatch;

SELECT levenshtein ('yhonsky','yhonky');

SELECT difference ('beard','bird');
*******************************************************************************************************
# Replicas
# Luego de tener los dos servidores montados
# Se realizan las siguientes configuraciones en la Master:
***postgresql.conf***
wal_level = hot_standby			# minimal, archive, or hot_standby
max_wal_senders = 2		# max number of walsender processes
archive_mode = on		# allows archiving to be done
archive_command = 'cp %p /tmp/%f'		# command to use to archive a logfile segment

***pg_hba.conf***
host	replication all 		<ip_replica/32>	  trust => Ojo con la identacion
host	replication all 		192.168.6.2/32		  trust => Ojo con la identacion

# Se realizan las siguientes configuraciones en la Replica:
***En consola***
sudo service postgresql stop

'rm -rf /var/lib/pgsql/data/*' => quitar las comillas para el comando

pg_basebackup -U webadmin -R -D /var/lib/pgsql/data/ --host=192.168.5.223 --port=5432

***postgresql.conf***
hot_standby = on			# "on" allows queries during recovery

Luego de hacer en enlace, la contraseña de la replica es la misma de master y hay que actualizarla.

sudo service postgresql status

sudo service postgresql start
*******************************************************************************************************


