SELECT COUNT(*) AS cantidad
FROM posts;

SELECT *
FROM usuarios
LEFT JOIN posts ON usuarios.id = posts.usuario_id;

SELECT *
FROM usuarios
LEFT JOIN posts ON usuarios.id = posts.usuario_id
WHERE posts.usuario_id IS NULL;

SELECT *
FROM usuarios
LEFT JOIN posts ON usuarios.id = posts.usuario_id
WHERE posts.usuario_id IS NULL;

SELECT *
FROM usuarios
RIGHT JOIN posts ON usuarios.id = posts.usuario_id
WHERE posts.usuario_id IS NULL;

#Intersección
SELECT *
FROM usuarios
INNER JOIN posts ON usuarios.id = posts.usuario_id;


#Union
FROM usuarios
LEFT JOIN posts ON usuarios.id = posts.usuario_id
UNION
SELECT *
FROM usuarios
RIGHT JOIN posts ON usuarios.id = posts.usuario_id;

#Diferencia simetrica
SELECT *
FROM usuarios
LEFT JOIN posts ON usuarios.id = posts.usuario_id
WHERE posts.usuario_id IS NULL
UNION
SELECT *
FROM usuarios
RIGHT JOIN posts ON usuarios.id = posts.usuario_id
WHERE posts.usuario_id IS NULL;



SELECT * FROM platziblog.posts
WHERE titulo LIKE '%escandalo%'
;

SELECT * FROM platziblog.posts
WHERE fecha_publicacion BETWEEN '2023-01-01' AND '2025-12-31'
;

SELECT * FROM platziblog.posts
WHERE YEAR(fecha_publicacion) BETWEEN '2023' AND '2024'
;

SELECT * FROM platziblog.posts
WHERE categoria_id IS NULL
;

SELECT * FROM platziblog.posts
WHERE categoria_id IS NOT NULL
;
